package jmi;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.OptionalInt;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;


class StreamTest {
   
   private static void header(String text) {
      System.out.printf("--------------- %s ---------------%n", text);
   }
   
   @Test
   void testInternalIterator() {
      header("testInternalIterator");
      
      List<Integer> elements = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
      
      elements.forEach(System.out::println);
      elements.stream()
              .forEach(System.out::println);
   }

   @Test
   void testBasicFilter() {
      header("testBasicFilter");
      
      IntStream elements = IntStream.range(1, 100);
      
      elements.filter(x -> x > 98)
              .forEach(System.out::println);
   }
   
   @Test
   void testCollectToMap() {
      header("testCollectToMap");
      
      Stream<String> elements = Stream.of("uno", "dos", "tres", "cuatro");
      
      Map<String, Integer> res = elements.collect(toMap(x -> x, String::length));
      
      res.entrySet()
         .stream()
         .forEach(x -> System.out.printf("%s -> %d%n", x.getKey(), x.getValue()));
   }
   
   @Test
   void testGroupBy() {
      header("testGroupBy");
      
      Stream<String> elements = Stream.of("uno", "dos", "tres", "cuatro");
      
      Map<Integer, List<String>> res = elements.collect(groupingBy(String::length));
      
      for(Entry<Integer, List<String>> entry : res.entrySet()) {
         System.out.printf("%d -> %s%n", entry.getKey(), entry.getValue());
      }
      
      System.out.println(res);
   }
   
   @Test
   void testMaxMin() {
      header("testMaxMin");
      
      Stream<String> elements = Stream.of("uno", "dos", "tres", "cuatro");
      
      int max = elements.map(String::length)
                        .max(Integer::compare)
                        .orElse(-1);
      
      System.out.printf("max -> %d%n", max);
   }
   
   @Test
   void testOptional() {
      header("testOptional");
      
      OptionalInt x = OptionalInt.of(200);
      
      assertTrue(x.isPresent(), "Value is not present");
      assertEquals(200, x.getAsInt());
   }

   @Test
   void testOptionalWithFilter() {
      header("testOptionalWithFilter");
      
   }
   
   @Test
   void testStringJoin() {
      Stream<String> elements = Stream.of("uno", "dos", "tres");
      
      String res = elements.collect(joining(","));
      
      System.out.println(res);
   }
   
   @Test
   void testSplitString() {
      header("testSplitString");
      
      String elements = "uno,dos,tres";
      
      Stream.of(elements.split(","))
            .forEach(System.out::println);
   }
   
   @Test
   void testPartitioning() {
      header("testPartitioning");
      
      IntStream elements = IntStream.range(0, 100);
      
      Map<Boolean, List<Integer>> res = elements
                                           .boxed()
                                           .collect(partitioningBy(x -> x % 2 == 0));
      
      res.entrySet().stream().forEach(entry -> {
         String values = entry.getValue()
                              .stream()
                              .map(String::valueOf)
                              .collect(joining(","));
         
         System.out.printf("%d -> %s%n", entry.getKey() ? 1 : 0 , values);
      });
   }
   
   @Test
   void testReducing() {
      header("testReducing");
      
      IntStream elements = IntStream.range(1, 100);
      
      int res = elements.filter(x -> x >= 50)
                        .reduce(0, Integer::sum);
      
      System.out.println(res);
   }
   
   @Test
   void testSorted() {
      header("testSorted");
      
      Stream<String> elements = Stream.of("abcde", "abcdef", "abcdefg", "abcdefgh", "a", "abc");
      
      elements.filter(x -> x.length() > 4)
              .sorted(reverseOrder())
              .forEach(System.out::println);
   }
   
   @Test
   void testFileStream() {
      try(Stream<String> stream = Files.lines(Paths.get("/etc/passwd"))) {
         
         String res = stream.filter(line -> line.toLowerCase()
                                                .contains("this"))
                                                .collect(joining("|"));
         
         System.out.println(res);
         
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   @Test
   void testParallelProcessing() {
      IntStream elements = IntStream.range(0, 10);
      
      int z = 100;
      
      IntConsumer work = x -> {
         try {
            Thread.sleep(1000);
         } catch (InterruptedException e) {
            e.printStackTrace();
         }
         
         System.out.println(x + ":" + z);
      };
      
      elements.parallel().forEach(work);
   }
   
   @Test
   void testFunction() {
      IntFunction<String> addAndToString = n -> String.valueOf(n);
      assertEquals("5", addAndToString.apply(5));
      
      IntFunction<IntPredicate> predicateFactory = n -> m -> n + m == 10;
      IntPredicate predicate = predicateFactory.apply(3);
      assertTrue(predicate.test(7));
      assertFalse(predicate.test(6));
   }
   
   @Test
   void testSum() {
      IntStream elements = IntStream.range(0, 10);
            
      assertEquals(45, elements.sum());
   }
   
   @Test
   void testCount() {
      IntStream elements = IntStream.range(0, 10);
      
      long total = elements.filter(x -> x % 2 == 0)
                           .count();
      
      assertEquals(5, total);
   }
   
   @Test
   void testMapInt() {
      Stream<String> elements = Stream.of("hola", "que", "tal");
      
      int total = elements.mapToInt(String::length)
                          .sum();
      
      assertEquals(10, total);
   }
   
   @Test
   void testReduceWithBaseElement() {
      IntStream elements = IntStream.range(0, 10);
      
      int maxElement = elements.reduce(20, (x, y) -> x > y ? x : y);
      
      assertEquals(20, maxElement);
   }
   
   @Test
   void testChars() {
      IntStream chars = "this is a beautiful test".chars();
      
      String res = chars.mapToObj(Integer::toString).collect(joining(","));

      assertEquals("116,104,105,115,32,105,115,32,97,32,98,101,97,117,116,105,102,117,108,32,116,101,115,116", res);
   }
   
   @Test
   void testSorted2() {
      Stream<String> elements = Arrays.stream("this is a beautiful string sentence".split(" "));
      
      List<String> res = elements.sorted((a, b) -> a.length() < b.length() ? -1 : 1).collect(Collectors.toList());
      
      List<String> expected = Arrays.asList("a", "is", "this", "string", "sentence", "beautiful");
      
      assertEquals(expected, res);
   }
   
   @Test
   void testComparator() {
      Comparator<String> cmpLength = (a, b) -> a.length() < b.length() ? -1 : 1;
      
      List<String> elements = Arrays.asList("este", "dialargo", "tan", "bonito", "tan");
      
      
      List<String> resultingList = elements.stream()
                                           .distinct()
                                           .sorted(cmpLength)
                                           .collect(Collectors.toList());

      List<String> expectedList = Arrays.asList("tan", "este", "bonito", "dialargo");
      
      assertEquals(expectedList, resultingList);
   }
   
   @Test
   void testComparatorAndThen() {
      Function<String, Integer> byLength = (a -> a.length());
      Function<String, String> byIdentity = (a -> a);
      
      List<String> elements = Arrays.asList("este", "dialargo", "tan", "bonita", "bonito", "tan");
      
      List<String> res = elements.stream()
                                 .sorted(comparing(byLength)
                                 .thenComparing(byIdentity))
                                 .collect(toList());
      
      List<String> expected = Arrays.asList("tan", "tan", "este", "bonita", "bonito", "dialargo");
      
      assertEquals(expected, res);
   }
   
   @Test
   void testFileListOnlyDirectories() {
      try {
         
         Files.list(Paths.get("."))
              .filter(Files::isDirectory)
              .forEach(System.out::println);
      
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   @Test
   void testFileSelection() {
      try {
         
         Files.newDirectoryStream(Paths.get("."),
                                  path -> !path.toString().endsWith(".java"))
              .forEach(System.out::println);
       
         File[] files = new File(".").listFiles(file -> file.isHidden());
         System.out.println(Arrays.toString(files));

         files = new File(".").listFiles(File::isHidden);
         System.out.println(Arrays.toString(files));

      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   @Test
   void testWatchFile() {
      try {
         
         Path path = Paths.get("/tmp");
         WatchService watchService = path.getFileSystem().newWatchService();
         
         path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
         
         System.out.println("Waiting for changes...");
         
         WatchKey watchKey = watchService.poll(1, TimeUnit.MINUTES);

         if (watchKey != null) {
            watchKey.pollEvents()
                    .stream()
                    .forEach(event -> System.out.println(event.context()));
         }
      
      } catch (IOException e) {
         e.printStackTrace();
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
   }
   
   @Test
   void testReadFileWithReaderAndSkip() {
      try(BufferedReader br = new BufferedReader(new FileReader("/etc/passwd"))) {

         br.lines()
           .skip(4)
           .findFirst()
           .ifPresent(System.out::println);
         
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      };         
   }
}
