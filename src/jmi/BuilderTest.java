package jmi;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class Data {
   public static class Builder {
      private int id;
      private String name;

      private Builder() {}
      
      public Builder setId(int id) {
         this.id = id;
         return this;
      }
      
      public Builder setName(String name) {
         this.name = name;
         return this;
      }
      
      public Data build() {
         return new Data(id, name);
      }
   }
   
   private int id;
   private String name;
   
   public Data(int id, String name) {
      this.id = id;
      this.name = name;
   }
   
   public static Builder builder() {
      return new Builder();
   }
   
   public int getId() {
      return this.id;
   }
   
   public String getName() {
      return this.name;
   }
}


class BuilderTest {

   @Test
   void testBuilder() {
      Data data = Data.builder()
                      .setId(10)
                      .setName("juan")
                      .build();
      
      assertEquals(10, data.getId());
      assertEquals("juan", data.getName());
   }
}