package jmi;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

class ArraysTest {

   public static int[] toIntArray(List<Integer> numbers) {
      int[] ret = new int[numbers.size()];
      
      for(int i = 0; i < numbers.size(); i++) {
         ret[i] = numbers.get(i);
      }
      
      return ret;
   }
   
   @Test
   void testMultidimensionalArray() {
      int [][] data = {
         {10, 20, 30},
         {40, 50, 60}
      }; 
   
      int []expected = {10, 20, 30, 40, 50, 60};
      
      // It'd be simpler to use an int[], but we want to play with the List interface.
      List<Integer> collected = new ArrayList<Integer>();
      
      for(int []i : data) {
         
         System.out.println(Arrays.toString(i));
         
         for(int j : i) {
            // We're aware of the autoboxing and accept it.
            collected.add(j);
         }
      }
      
      // Array check.
      assertEquals(collected.size(), expected.length);
      assertArrayEquals(expected, toIntArray(collected));
      
      // List check.
      List<Integer> expectedList = Arrays.stream(expected)
                                         .boxed()
                                         .collect(Collectors.toList());

      assertEquals(expectedList.size(), collected.size());
      assertEquals(expectedList, collected);
   }
}
